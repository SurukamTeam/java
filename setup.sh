tar -xvzf jdk-8u212-linux-x64.tar.gz
export PATH="$PATH:/usr/lib/jvm/jdk1.8.0_212/bin:/usr/lib/jvm/jdk1.8.0_212/db/bin:/usr/lib/jvm/jdk1.8.0_212/jre/bin"
export JAVA_HOME="/usr/lib/jvm/jdk1.8.0_212"
update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_212/bin/java" 0
update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_212/bin/javac" 0
update-alternatives --set java /usr/lib/jvm/jdk1.8.0_212/bin/java
update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_212/bin/javac
update-alternatives --list java
update-alternatives --list javac
java -version